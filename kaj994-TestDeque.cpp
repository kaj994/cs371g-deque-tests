// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;};

using
    deque_types =
    Types<
           deque<int>,
           my_deque<int>,
           deque<int, allocator<int>>,
           my_deque<int, allocator<int>>>;

#ifdef __APPLE__
    TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
    TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    x.push_back(3);
    ASSERT_EQ(x[0], 3);
}

TYPED_TEST(DequeFixture, test3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.push_back(3);
    ASSERT_EQ(x[10], 3);
}

TYPED_TEST(DequeFixture, test4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    for (int i = 0; i < 90; ++i)
    {
        x.push_back(3);
    }
    ASSERT_EQ(x[99], 3);
    ASSERT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture, test5) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.push_front(4);
    ASSERT_EQ(x[0], 4);
    ASSERT_EQ(x.size(), 11);
}

TYPED_TEST(DequeFixture, test6) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    for (int i = 0; i < 90; ++i)
    {
        x.push_front(3);
    }
    ASSERT_EQ(x[99], 2);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture, test7) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 2);
    x.pop_back();
    x.push_back(3);
    ASSERT_EQ(x[98], 2);
    ASSERT_EQ(x[99], 3);
    ASSERT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture, test8) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 2);
    x.pop_front();
    x.push_front(3);
    ASSERT_EQ(x[0], 3);
    ASSERT_EQ(x[99], 2);
    ASSERT_EQ(x.size(), 100);
}


TYPED_TEST(DequeFixture, test9) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 2);
    for (int i = 0; i < 90; ++i)
    {
        x.pop_back();
    }
    ASSERT_EQ(x.size(), 10);
}

TYPED_TEST(DequeFixture, test10) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(100, 2);
    for (int i = 0; i < 90; ++i)
    {
        x.pop_front();
    }
    ASSERT_EQ(x.size(), 10);
}

TYPED_TEST(DequeFixture, test23) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.push_front(3);
    x.push_back(3);
    ASSERT_EQ(x.front(), 3);
    ASSERT_EQ(x.back(), 3); 
}

TYPED_TEST(DequeFixture, test24) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.pop_front();
    x.pop_back();
    ASSERT_EQ(x.size(), 8);
}

TYPED_TEST(DequeFixture, test25) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.pop_front();
    x.pop_back();
    x.push_front(3);
    ASSERT_EQ(x.size(), 9);
    ASSERT_EQ(x.front(), 3);
}

TYPED_TEST(DequeFixture, test26) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.pop_front();
    x.pop_back();
    x.push_front(3);
    ASSERT_EQ(x.size(), 9);
    ASSERT_EQ(x.front(), 3);
}

TYPED_TEST(DequeFixture, test27) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.resize(100, 2);
    x.push_front(3);
    x.push_back(3);
    ASSERT_EQ(x.size(), 102);
    ASSERT_EQ(x.front(), 3);
    ASSERT_EQ(x.back(), 3);
}


TYPED_TEST(DequeFixture, test11) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    deque_type y(10, 2);
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, test12) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    deque_type y(9, 2);
    ASSERT_TRUE(x > y);
}

TYPED_TEST(DequeFixture, test13) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.resize(100);
    ASSERT_EQ(x.size(), 100);
}

TYPED_TEST(DequeFixture, test14) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.resize(5);
    ASSERT_EQ(x.size(), 5);
}

TYPED_TEST(DequeFixture, test15) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    deque_type y(15, 3);
    x.swap(y);
    ASSERT_EQ(y.size(), 10);
    ASSERT_EQ(x.size(), 15);
}

TYPED_TEST(DequeFixture, test16) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    while (iter != end)
    {
        *iter;
        ++iter;
    }
    ASSERT_TRUE(true);
}

TYPED_TEST(DequeFixture, test17) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    while (iter != end)
    {
        *iter = 3;
        ++iter;
    }
    ASSERT_EQ(x[9], 3);
}

TYPED_TEST(DequeFixture, test18) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    --end;
    while (iter <= end)
    {
        *end = 3;
        --end;
    }
    ASSERT_EQ(x[0], 3);
}

TYPED_TEST(DequeFixture, test19) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    deque_type y(5, 2);
    ASSERT_TRUE(x > y);
}

TYPED_TEST(DequeFixture, test20) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    for(int i = 0; i < 1000; ++i) {
        x.push_back(2);
    }
    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    while (iter != end)
    {
        *iter = 3;
        ++iter;
    }
    ASSERT_EQ(x[999], 3);
}

TYPED_TEST(DequeFixture, test21) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    x.resize(100, 2);
    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    while (iter != end)
    {
        *iter = 3;
        ++iter;
    }
    ASSERT_EQ(x[99], 3);
}

TYPED_TEST(DequeFixture, test22) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);

    typename deque_type::iterator iter = x.begin();
    typename deque_type::iterator end = x.end();
    while (iter != end)
    {
        *iter = 3;
        ++iter;
    }
    ASSERT_EQ(x[9], 3);
}

TYPED_TEST(DequeFixture, test28) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x(10, 2);
    typename deque_type::iterator iter = x.begin();
    ++++++++iter;
    x.insert(iter, 3);
    ASSERT_EQ(x[4], 3);
    ASSERT_EQ(*iter, 2);
    ASSERT_EQ(x.size(), 11);
}

TYPED_TEST(DequeFixture, test29) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 10; ++i)
    {
        x.push_back(i);
        ASSERT_EQ(x[i], i);
    }
}

TYPED_TEST(DequeFixture, test30) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x;
    for (int i = 0; i < 10; ++i)
    {
        x.push_back(i);
        ASSERT_EQ(x.at(i), i);
    }

}

TYPED_TEST(DequeFixture, test31) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(10, 2);
    for (int i = 0; i < 10; ++i)
    {
        ASSERT_EQ(x[i], 2);
        ASSERT_EQ(x.at(i), 2);
    }

}


// TYPED_TEST(DequeFixture, test2) {
//     using deque_type = typename TestFixture::deque_type;
//     using iterator   = typename TestFixture::iterator;
//     deque_type x;
//     iterator b = x.begin();
//     iterator e = x.end();
//     ASSERT_EQ(b, e);}

// TYPED_TEST(DequeFixture, test3) {
//     using deque_type     = typename TestFixture::deque_type;
//     using const_iterator = typename TestFixture::const_iterator;
//     const deque_type x;
//     const_iterator b = x.begin();
//     const_iterator e = x.end();
//     ASSERT_EQ(b, e);}
